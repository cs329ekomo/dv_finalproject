#ui.R 
require(shiny)
require(shinydashboard)
require(leaflet)

dashboardPage(
  dashboardHeader(
  ),
  dashboardSidebar(
    sidebarMenu(
      menuItem("BoxPlot", tabName = "boxplot", icon = icon("th")),
      menuItem("Scatter Plot", tabName = "scatter", icon = icon("th")),
      menuItem("Histogram", tabName = "histogram", icon = icon("th"))
    )
  ),
  dashboardBody(
    tabItems(
      # First tab content
      tabItem(tabName = "boxplot",
              sliderInput("YEAR", "Select Years:", 
                          min = 1995, max = 2010,  value = 2010, step = 1),
              actionButton(inputId = "clicks1",  label = "Click me"),
              plotOutput("distPlot1")
      ),
      
      tabItem(tabName = "scatter",
              radioButtons("YEARSS", "Select Years:",
                           c("1995" = 1995,
                             "2000" = 2000,
                             "2005" = 2005,
                             "2010" = 2010,
                             "2012" = 2012)),
              actionButton(inputId = "clicks2",  label = "Click me"),
              plotOutput("distPlot2")
      ),
      
      tabItem(tabName = "histogram",
              selectInput("YEARS", "Select Years:",
                          c("1995" = 1995,
                            "2000" = 2000,
                            "2005" = 2005,
                            "2010" = 2010)),
              actionButton(inputId = "clicks3",  label = "Click me"),
              plotOutput("distPlot3")
      )
      
      
      
      
    )
  )
)



